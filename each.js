
function each(elements, cb) {
    // Do NOT use forEach to complete this function.
    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // This only needs to work with arrays.
    // You should also pass the index into `cb` as the second argument
    // based off http://underscorejs.org/#each
    if(!elements || !Array.isArray(elements) || !elements.length || cb instanceof Function === false){
      console.log('not valid input')
      return
    }
    for(let counter = 0; counter < elements.length; counter++){
      cb(elements[counter],counter);
    }
}
module.exports = each
// each(items,printIndex);
