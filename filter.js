
function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
    if(!elements || !Array.isArray(elements) || !elements.length || cb instanceof Function === false){
      return 'not valid input'
    }
    let array = [];
    for(let counter = 0; counter < elements.length; counter++){
      let value = cb(elements[counter]);
      if(value){
        array.push(elements[counter]);
      }
    }
    return array;
}
module.exports = filter
