function find(elements, cb) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
    if(!elements || !Array.isArray(elements) || !elements.length || cb instanceof Function === false){
      return 'not valid input'
    }
    for(let counter = 0; counter < elements.length; counter++){
      let value = cb(elements[counter]);
      if(value){
        return elements[counter];
      }
    }
}
module.exports = find
