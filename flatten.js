
function flatten(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    if(!elements || !Array.isArray(elements) || !elements.length){
      return 'not valid input'
    }
    let flattenedArray = [];
    for(let counter = 0; counter < elements.length; counter++){
      if(Array.isArray(elements[counter])){
        let returnedArray = flatten(elements[counter]);
        flattenedArray = flattenedArray.concat(returnedArray);
      }
      else{
        flattenedArray.push(elements[counter]);
      }
    }
    return flattenedArray;
}
module.exports = flatten
