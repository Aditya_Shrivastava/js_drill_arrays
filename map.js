
function map(elements, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
    if(!elements || !Array.isArray(elements) || !elements.length || cb instanceof Function === false){
      return 'not valid input'
    }
    let array = [];
    for(let counter = 0; counter < elements.length; counter++){
      let returnValue;
      returnValue = cb(elements[counter]);
      array.push(returnValue);
    }
    return array;
}
module.exports = map
