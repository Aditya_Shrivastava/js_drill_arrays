
function reduce(elements, cb) {
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
    if(!elements || !Array.isArray(elements) || !elements.length || cb instanceof Function === false){
      return 'not valid input'
    }
    let result = elements[0];
    for(let counter = 1; counter < elements.length; counter++){
      result = cb(result,elements[counter]);
    }
    return result;
}
module.exports = reduce
