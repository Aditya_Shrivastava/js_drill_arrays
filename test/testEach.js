const each = require('../each.js')
const printIndex = (element,index) =>{
  console.log(`The index of element ${element} is ${index}`);
};
const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.
let print

each(items,printIndex)
each([],printIndex)
each('items',print)
each(items, 'printIndex')
