const filter = require('../filter.js')
const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.
const isPerfectSquare = (element) => {
  let squareRoot = Math.sqrt(element);
  if(Math.ceil(squareRoot) === Math.floor(squareRoot)){
    return true;
  }
  else{
    return false;
  }
}
let callback
let filteredArray = filter(items,isPerfectSquare)
console.log(filteredArray)
console.log(filter([],isPerfectSquare))
console.log(filter('items',callback))
console.log(filter(items,callback))
