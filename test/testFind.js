const find = require('../find.js')
const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.
const isPerfectSquare = (element) => {
  let squareRoot = Math.sqrt(element);
  if(Math.ceil(squareRoot) === Math.floor(squareRoot)){
    return true;
  }
  else{
    return false;
  }
}
let variable = 'some value'
let result = find(items,isPerfectSquare);
console.log(result);
console.log(find([],isPerfectSquare))
console.log(find('items',variable))
console.log(find(items, variable))
