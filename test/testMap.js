const map = require('../map.js')
const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.
const multiplyByHundred = (element) =>{
  return element*100;
}
let newArray = map(items,multiplyByHundred);
console.log(newArray);
console.log(map([],multiplyByHundred))
console.log(map('items',items))
console.log(map([],'multiplyByHundred'))
console.log(map(null, multiplyByHundred))
console.log(map(items,'multiplyByHundred'))
