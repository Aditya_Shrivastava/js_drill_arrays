const reduce = require('../reduce.js')
const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.
const product = (startingValue,element) => {
  return startingValue*element;
}

let reducedValue = reduce(items,product);
console.log(reducedValue);
console.log(reduce([],product))
console.log(reduce('items',product))
console.log(reduce(items, 'product'))
console.log(reduce([2,3,1,4],product))
